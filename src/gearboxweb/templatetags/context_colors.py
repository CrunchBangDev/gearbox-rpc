import colorsys

from django import template  # type: ignore

register = template.Library()


@register.simple_tag
def context_pallette(contexts):
    hsv = [(x * 1.0 / len(contexts), 1, 1) for x in range(len(contexts))]
    colors = map(lambda x: colorsys.hsv_to_rgb(*x), hsv)
    hex_colors = (tuple(round(v * 75 + 180) for v in x) for x in colors)
    for context in contexts:
        yield context, "%02X%02X%02X" % next(hex_colors)
