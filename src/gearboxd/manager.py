import importlib
import logging
from typing import Dict, List, Optional, Type

from django.conf import settings  # type: ignore
from django.db import OperationalError, close_old_connections, models  # type: ignore

from gearboxlib.models import CogMemory  # type: ignore
from gearboxlib.templates import Cog  # type: ignore

from .coggraph import CogGraph  # type: ignore
from .models import CogRegistration  # type: ignore
from .scheduler import CogScheduler  # type: ignore


class CogManager:
    """Manages cog registrations and state"""

    log = logging.getLogger("gearbox.cog_manager")
    cogs: Dict[str, Type[Cog]] = {}
    running_cogs: Dict[str, Cog] = {}
    data_emitted: int = 0
    graph: CogGraph = CogGraph()

    def __init__(self):
        # Get all existing cog registrations and start scheduler
        self.registry = CogRegistration.objects.all()
        self.scheduler = CogScheduler(self)

    @staticmethod
    def get_registration(cog_name: str) -> Optional[CogRegistration]:
        """
        Try to get an existing registration for a given cog

        Args:
            cog_name (str): The name of the cog to look up

        Returns:
            Optional[CogRegistration]: Either the cog's registration or None
        """
        try:
            return CogRegistration.objects.filter(name=cog_name)[0]
        except IndexError:
            return None

    def get_or_create_registration(self, cog_name: str) -> CogRegistration:
        """
        Try to get a cog's registration or create one if it doesn't exist

        Args:
            cog_name (str): The name of the cog to look up

        Returns:
            CogRegistration: The cog's registration
        """
        if not (registration := self.get_registration(cog_name)):
            registration = CogRegistration(name=cog_name)
        return registration

    def register(self, cog_class) -> CogRegistration:
        """
        Add a cog to the cog registry, and the cog list, and the cog graph

        Args:
            cog_class (Cog):

        Returns:
            CogRegistration: The cog's registration
        """
        self.cogs[cog_class.__name__] = cog_class
        self.graph.add(cog_class)
        registration = self.get_or_create_registration(cog_class.__name__)
        registration.save()
        return registration

    @staticmethod
    def _publish_historical_payloads(cog: Type[Cog]) -> None:
        """
        Send a cog all of the subscribed topics created since it last ran

        Args:
            cog (Type[Cog]): An initialized cog ready to receive data payloads
        """
        topics: List[models.Model] = []
        if not cog.collects:
            return None
        if isinstance(cog.collects, tuple):
            topics.extend(cog.collects)
        else:
            topics.append(cog.collects)
        for topic in topics:
            try:
                latest_record = CogMemory.objects.filter(
                    cog=cog.__class__.__name__
                ).latest("recorded")
            except CogMemory.DoesNotExist:
                continue
            for payload in topic.objects.filter(recorded__gte=latest_record.recorded):
                cog._receive(payload)

    def setup_cog(self, cog: Type[Cog]) -> bool:
        """
        Run a cog's setup method and catch it up on missed work

        Args:
            cog (Type[Cog]): An initialized cog ready to be set up

        Returns:
            bool: Whether the setup process was successful
        """
        cog_name = cog.__class__.__name__
        if cog.setup() is False:
            self.log.error(f"Failed setting up cog: {cog_name}")
            self._set_cog_error(cog_name)
            return False
        # Catch the cog up on missed work
        self._publish_historical_payloads(cog)
        self.running_cogs[cog_name] = cog
        return True

    def allowed_to_run(self, registration: CogRegistration) -> bool:
        """
        Determine based on a cog's registration whether it is allowed to run

        Args:
            registration (CogRegistration): The registration to be checked

        Returns:
            bool: Allowed to run or not
        """
        if not (allowed := registration.enabled and not registration.error):
            reason = "disabled" if not registration.enabled else "error state"
            self.log.debug(f"Cog {registration.name} cannot run ({reason})")
        return allowed

    def able_to_run(self, cog: Cog, registration: CogRegistration) -> bool:
        """
        Determine whether a cog is able and allowed to run

        Args:
            cog (Cog): The cog to potentially be run
            registration (CogRegistration): The registration to be checked

        Returns:
            bool: Able to run or not
        """
        return not any(
            (
                cog.schedule is False,
                not hasattr(cog, "main"),
                not self.allowed_to_run(registration),
            )
        )

    def load_cog(self, name: str) -> bool:
        """
        Load a cog into the job queue

        Args:
            name (str): Name of the cog to be loaded

        Returns:
            bool: Success
        """
        # Unregistered cogs can't be loaded
        if not (registration := self.get_registration(name)):
            return False
        # Check cog's run capability and current permission to run
        if not self.allowed_to_run(registration):
            return False
        # Fetch the cog class to be loaded
        cog_class: Cog = self.cogs[name]
        # Refuse to load already-loaded cogs
        if name in self.running_cogs.keys():
            self.log.warn(f"Cog {name} is already loaded")
            return False
        # Clear any interrupts blocking this cog
        self.scheduler.uninterrupt(name)
        # Initialize, set up, schedule, report back, and return success
        cog = cog_class()
        if not self.setup_cog(cog):
            return False
        self.scheduler.run_cog(cog)
        self.log.info(f"Loaded cog {name}")
        return True

    def unload_cog(self, name: str) -> bool:
        """
        Unload a cog from the job queue

        Args:
            name (str): Name of the cog to be unloaded

        Returns:
            bool: Success
        """
        # Remove the cog from the running list
        try:
            cog = self.running_cogs.pop(name)
        except KeyError:
            self.log.warn(f"No cog named {name} available to unload")
            return False
        # Remove the job from the queue
        self.scheduler.unschedule(name)
        # Perform any teardown specified by the cog, report back, and return success
        cog.unsetup()
        self.log.info(f"Unloaded {name}")
        return True

    def reload_cog(self, name: str) -> bool:
        """
        Reload a cog from disk and load it into the job queue

        Args:
            name (str): Name of the cog to be reloaded

        Returns:
            bool: Success
        """
        # Fetch the cog class to be reloaded
        cog_class: Cog = self.cogs[name]
        # Get and reload the cog's package
        package_name = cog_class._package
        package = importlib.import_module(f"{package_name}.cogs")
        importlib.reload(package)
        self.log.info(f"Reloaded cog package {package_name}")
        # Get replacement cog class from the reloaded package
        cog_class = getattr(package, name)
        cog_class._package = package_name
        # Terminate any existing (old) copies of the cog
        if name in self.running_cogs.keys():
            self.unload_cog(name)
        # Overwrite the stored copy of the cog class
        self.cogs[name] = cog_class
        # Attempt to queue the reloaded cog
        return self.load_cog(name)

    def _set_cog_error(self, name):
        """
        Set a cog's registration error flag to true

        Args:
            name ([type]): Name of the cog to be altered
        """
        CogRegistration.objects.filter(name=name).update(error=True)
        self.unload_cog(name)

    def clear_cog(self, name):
        """
        Set a cog's registration error flag to false

        Args:
            name ([type]): Name of the cog to be altered
        """
        CogRegistration.objects.filter(name=name).update(error=False)
        self.scheduler.uninterrupt(name)

    @staticmethod
    def enable_cog(name):
        """
        Set a cog's registration enabled flag to true

        Args:
            name ([type]): Name of the cog to be altered
        """
        CogRegistration.objects.filter(name=name).update(enabled=True)

    def disable_cog(self, name):
        """
        Set a cog's registration enabled flag to false

        Args:
            name ([type]): Name of the cog to be altered
        """
        if name in self.running_cogs.keys():
            self.unload_cog(name)
        CogRegistration.objects.filter(name=name).update(enabled=False)

    @staticmethod
    def remove_cog(name: str) -> bool:
        """
        Delete a cog's registration

        Args:
            name ([type]): Name of the cog to be forgotten
        """
        CogRegistration.objects.filter(name=name).delete()
        return True
