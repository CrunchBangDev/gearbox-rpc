import logging
import time
from concurrent.futures import FIRST_COMPLETED, Future, ThreadPoolExecutor, wait
from concurrent.futures._base import CancelledError
from dataclasses import dataclass, field
from datetime import datetime, timezone
from heapq import heappop, heappush
from threading import Event
from typing import Dict, List, Optional, Tuple, Union

from pubsub import pub  # type: ignore

from gearboxlib.models import CogMemory  # type: ignore
from gearboxlib.templates import Cog  # type: ignore

from .models import CogRegistration  # type: ignore


# Scheduled cog jobs must be comparable for heap reasons
@dataclass(order=True)
class ScheduledCog:
    cog: Cog = field(compare=False)
    requeue_time: float


class CogScheduler:
    """Manages the job pool and handles cog scheduling"""

    log = logging.getLogger("gearbox.cog_scheduler")
    pool: ThreadPoolExecutor
    # The jobs map provides a named index into scheduled job futures
    jobs: Dict[str, Future] = {}
    # The interrupts map is indexed by either cog name or job future
    interrupts: Dict[Union[str, Future], Event] = {}
    # The job queue is used as a min heap
    queued: List[ScheduledCog] = []
    # Queue directory values should be references to heap entries
    queue_directory: Dict[str, ScheduledCog] = {}

    def __init__(self, manager):
        self.manager = manager

    def main(self):
        """Implements the main loop of the GearBox program"""
        # Set up interrupt event for self
        self.interrupts["daemon"] = Event()
        # Start concurrent thread pool
        with ThreadPoolExecutor() as self.pool:
            # Start cogs
            for cog in self.manager.running_cogs.values():
                self.run_cog(cog)
            # Set up forever loop
            while True:
                # Await completion of any future
                while self.jobs:
                    for done in wait(
                        [*self.jobs.values()], return_when=FIRST_COMPLETED
                    ).done:
                        # Cog exceptions handled by _spawn_worker, so this means core
                        try:
                            e = done.exception()
                        except CancelledError:
                            self.remove_job(done)
                            continue
                        except Exception as e:
                            self.log.exception("Unhandled core exception!", exc_info=e)
                            self.remove_job(done)
                            continue
                        # Unpack results
                        cog, requeue_time = done.result()
                        if not cog:
                            self.remove_job(done)
                            continue
                        name = cog.__class__.__name__
                        del self.jobs[name]
                        # Check if job was cancelled
                        if self.interrupts.pop(done).is_set():
                            continue
                        # Check if cog is cancelled
                        if self.interrupts.get(name, Event()).is_set():
                            continue
                        # None is a sign that we should not requeue the job
                        if not requeue_time:
                            continue
                        self.add_to_heap(cog, requeue_time)
                        self.reschedule()
                # Kill some time if appropriate
                if self.queued:
                    self.wait()
                else:
                    self.interrupts["daemon"].wait(1)
                # Requeue scheduled jobs before returning to awaiting futures
                self.reschedule()

    def remove_job(self, job: Future):
        # Rebuild job queue without the indicated job
        self.jobs = {k: v for k, v in self.jobs.items() if v != job}

    def add_to_heap(self, cog: Cog, requeue_time: float):
        """
        Add a scheduled job to the job heap queue to be run later

        Args:
            cog (Cog): An initialized and set up cog ready to be run
            requeue_time (float): The time that the cog should run next

        Raises:
            ValueError: The cog cannot be an already-scheduled cog
        """
        name = cog.__class__.__name__
        if name in self.queue_directory.keys():
            raise ValueError(f"A cog with this name is already scheduled: {name}")
        scheduled_job = ScheduledCog(cog, requeue_time)
        self.queue_directory[name] = scheduled_job
        heappush(self.queued, scheduled_job)

    def run(self, cog: Cog, *args) -> Tuple[Cog, Optional[float]]:
        """
        Try to run a cog

        Args:
            cog (Cog): An initialized and set up cog ready to be run

        Returns:
            Tuple[Cog, Optional[float]]: A cog and its next scheduled run time
        """
        name = cog.__class__.__name__
        if not (registration := self.manager.get_registration(name)):
            return cog, None
        if self.manager.able_to_run(cog, registration):
            return self.schedule(cog, registration, *args)
        else:
            return cog, None

    def run_cog(self, cog, *args, **kwargs):
        """
        Submit a cog to the job pool

        Args:
            cog ([type]): An intialized and set up cog ready to be run

        Raises:
            ValueError: The cog cannot be an already-scheduled cog
        """
        name = cog.__class__.__name__
        if name in self.jobs.keys() and not self.interrupts[self.jobs[name]].is_set():
            raise ValueError(f"A cog with this name is already scheduled: {name}")
        job = self.pool.submit(self.run, cog, *args, **kwargs)
        # Store a named reference to the job
        self.jobs[name] = job
        # Create a unique interrupt just for this job
        self.interrupts[job] = Event()

    def reschedule(self):
        """Push any scheduled jobs from the job heap queue to the job pool"""
        while self.queued and self.queued[0].requeue_time <= time.time():
            if not (cog := heappop(self.queued).cog):
                # Job must have been invalidated
                continue
            del self.queue_directory[cog.__class__.__name__]
            self.run_cog(cog)
        # print(len(self.jobs.keys()), len(self.queued))

    def wait(self, minimum: int = 0, maximum: int = 1):
        """
        Sleep until next scheduled job, applying minimum and maximum constraints

        Args:
            minimum (int, optional): Minimum time to wait for. Defaults to 0.
            maximum (int, optional): Maximum time to wait for. Defaults to 1.
        """
        queue_backoff = self.queued[0].requeue_time - time.time()
        # Constrain backoff based on minimum and maximum
        real_backoff = min(max(queue_backoff, minimum), maximum)
        if self.interrupts["daemon"].wait(real_backoff):
            self.log.info("Received interrupt while waiting to do work")

    def _spawn_worker(self, cog: Cog, interrupt: Event) -> Cog:
        """
        Run a cog and handle any errors or falsey returns

        Args:
            cog (Cog): An intialized and set up cog ready to be run
            interrupt (Event): A unique event associated with this cog

        Returns:
            Cog: The cog that was run
        """
        name = cog.__class__.__name__
        try:
            result = cog.main(interrupt)
        except Exception as e:
            self.log.exception(f"Unhandled cog error in {name}!", exc_info=e)
            self.manager._set_cog_error(name)
            return cog
        if result is False:
            self.log.error(f"Cog {name} failed and will now {cog.on_failure}")
            if cog.on_failure == "halt":
                self.manager._set_cog_error(name)
            elif cog.on_failure == "restart":
                self.manager.unload_cog(name)
                # Fork load_cog to a new thread because we are now cancelled
                self.pool.submit(self.manager.load_cog, name)
        return cog

    def schedule(self, cog: Cog, reg: CogRegistration) -> Tuple[float, Optional[Cog]]:
        """
        Determine the time when a cog should run, or run it if that time is past

        Args:
            cog (Cog): An intialized and set up cog ready to be run
            reg (CogRegistration): The cog's registration data

        Returns:
            Tuple[float, Optional[Cog]]: A cog and its next scheduled run time
        """
        # Create and track interrupt signal for cog if not defined
        if not (interrupt := self.interrupts.get(reg.name, None)):
            self.interrupts[reg.name] = interrupt = Event()
        if interrupt.is_set():
            return None, None
        if reg.last_run:
            # Calculate next scheduled run time
            now = datetime.now(tz=timezone.utc)
            backoff = (reg.last_run - now).total_seconds() + cog.schedule
            if backoff > 0:
                return cog, time.time() + backoff
        result: Optional[Cog] = self._spawn_worker(cog, interrupt)
        # Update last_run value
        CogRegistration.objects.filter(name=reg.name).update(
            last_run=datetime.now(tz=timezone.utc)
        )
        return result, time.time() + cog.schedule

    def interrupt(self, name: str):
        """
        Set a cog's interrupt event

        Args:
            name (str): The name of the cog to be interrupted
        """
        try:
            self.interrupts[name].set()
        except KeyError:
            pass

    def uninterrupt(self, name: str):
        """
        Delete a cog's interrupt event

        Args:
            name (str): The name of the cog to be uninterrupted
        """
        try:
            del self.interrupts[name]
        except KeyError:
            pass

    def unschedule(self, name: str) -> bool:
        """
        Remove a job from the job heap queue and/or the job pool

        Args:
            name (str): The name of the cog to be unscheduled

        Returns:
            bool: Success
        """
        self.interrupt(name)
        # Try to remove the job from the queue
        success = self.invalidate_queued_job(name)
        # If that didn't work, try to set the interrupt for the running job
        if not success:
            try:
                # Cancel the job and set its unique interrupt event
                job = self.jobs[name]
                self.interrupts[job].set()
                job.cancel()
                success = True
            except KeyError:
                pass
        # Job could have been running but stopped while we were checking
        if not success:
            success = self.invalidate_queued_job(name)
        return success

    def invalidate_queued_job(self, name: str) -> bool:
        """
        Replace a queued job's worker with nothing to invalidate that job

        Args:
            name (str): The name of the cog whose job should be invalidated

        Returns:
            bool: Success
        """
        try:
            job = self.queue_directory.pop(name)
        except KeyError:
            return False
        job.cog = None
        return True

    def run_now(self, name: str) -> bool:
        """
        Run a job immediately, regardless of its schedule

        Args:
            name (str): The name of the cog whose job should be run

        Returns:
            bool: Success
        """
        if name in self.jobs.keys():
            self.log.warning(f"Tried to run {name} while it was already running")
            return False
        self.pool.submit(self._spawn_worker, self.manager.running_cogs[name], Event())
        return True
