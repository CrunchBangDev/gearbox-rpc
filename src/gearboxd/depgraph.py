import operator
import re
from collections import namedtuple
from typing import Callable, Generator, Sequence, Set, Tuple, Union

# Packages are the nodes in the graph
Package = namedtuple("Package", "name version")
# Requirements are the edges in the graph
Requirement = namedtuple("Requirement", "required_by requires comparator version")
# Basic semantic incrementing sequence version representation
Version = namedtuple("Version", "major minor patch", defaults=(0, 0, 0))

# Matches <, >, <=, >=, =, ==, ~=, and a few other things not worth excluding
REQUIREMENT_TOKENIZER = re.compile(r"([<=~>]=?)")

# Comparison operator lookup table because eval is for losers
COMPARATORS = {
    "~=": lambda a, b: a[2] >= b[2] and a[:2] == b[:2],
    "==": operator.eq,
    ">=": operator.ge,
    "<=": operator.le,
    "=": operator.eq,
    ">": operator.gt,
    "<": operator.lt,
}


def normalize_version(version: Sequence[int]) -> Version:
    """
    Normalize a version tuple, ensuring it has at least and only 3 entries

    Args:
        version (Sequence[int]): A sequence of integers representing a version

    Returns:
        Version:  A namedtuple storing a set of major, minor, and patch versions
    """
    # Cut input to 3 values, zerofill is done by namedtuple defaults
    return Version(*[int(x) for x in version][:3])


def parse_version(version: str) -> Version:
    """
    Parse a simple version string into a Version namedtuple

    Args:
        version (str): A version string consisting of numbers and periods

    Returns:
        Version: A namedtuple storing a set of major, minor, and patch versions
    """
    return normalize_version([int(x) for x in str(version).split(".")])


def get_comparator(
    comparator_string: str, invert: bool = False
) -> Callable[[Version, Version], bool]:
    """
    Gets the logical comparison operator associated with the given string, or
    its logical inverse

    Args:
        comparator_string (str): A comparison operator
        invert (bool, optional): Returns the logical inverse of the given comparator

    Raises:
        ValueError: Comparator not one of: <, >, <=, >=, =, ==, ~=

    Returns:
        Callable[[Version, Version], bool]: [description]
    """
    if invert:
        # Swap < and > characters to invert comparator string
        comparator_string = comparator_string.translate({62: 60, 60: 62})
    try:
        return COMPARATORS[comparator_string]
    except KeyError:
        raise ValueError(f"Invalid comparator: {comparator_string}")


def parse_requirement(
    dependency: str,
) -> Generator[Tuple[str, Callable[[Version, Version], bool], Version], None, None]:
    """
    Parse a package requirement string into a package name and its version
    requirement information

    Args:
        dependency (str): A string such as "1.6.2<=foo<=2" or "bar == 1.3.7"

    Raises:
        ValueError: Given dependency string had too many or too few tokens

    Yields:
       Tuple[str, str, Version]: Package name, version comparator, and version
    """
    req = REQUIREMENT_TOKENIZER.split(str(dependency))
    if len(req) == 1:
        # Add a comparator and version if none specified
        yield req[0].strip(), operator.ge, Version()
    elif len(req) == 3:
        try:
            # Break requirement into package name, comparator, and version
            yield req[0].strip(), get_comparator(req[1]), parse_version(req[2])
        except ValueError:
            # Order might be reversed (left-hand comparison only)
            yield req[2].strip(), get_comparator(req[1], True), parse_version(req[0])
    elif len(req) == 5:
        yield req[2].strip(), get_comparator(req[1], True), parse_version(req[0])
        # The second part can be evaluated as usual
        yield req[2].strip(), get_comparator(req[3]), parse_version(req[4])
    else:
        raise ValueError(
            f"Unacceptable number of tokens in dependency ({len(req)}): {dependency}"
        )


class DependencyGraph:
    """
    Constructs a directed graph of packages and their dependencies in order to
    identify unmet requirements

    Raises:
        KeyError: Looking up a node using an invalid name
        ValueError: Attempting to resolve dependencies with invalid comparators
    """

    nodes: Set[Package] = set()
    edges: Set[Requirement] = set()

    def add(self, name: str, version: Union[Version, str]) -> None:
        """
        Add a node to the graph

        Args:
            name (str): Name for the node
            version (Union[Version, str]): A Version object or string representation of the version
        """
        v: Version = version if isinstance(version, Version) else parse_version(version)
        self.nodes.add(Package(name, v))

    def link(self, name: str, dependencies: Sequence[str]) -> None:
        """
        Create an edge between two nodes

        Args:
            name (str): The name of the node being linked
            dependencies (Sequence[str]): A sequence of dependency strings for the node
        """
        errors = []
        for d in dependencies:
            # Unpack required package, comparator, and version for each requirement
            try:
                self.edges.update(Requirement(name, *r) for r in parse_requirement(d))
            except ValueError as e:
                # Defer error until function completes
                errors.append(e)
        if errors:
            raise Exception(errors)  # Raise deferred dependency parsing errors

    def node_by_name(self, name: str) -> Package:
        """
        Fetch a node from the graph by its name

        Args:
            name (int): The name of a node to fetch from the graph

        Raises:
            KeyError: Name was not located in the graph

        Returns:
            Package
        """
        try:
            return next(filter(lambda node: node.name == name, self.nodes))
        except StopIteration:
            raise KeyError(f"No node with name: {name}")

    def resolve_edge(self, edge: Requirement) -> bool:
        """
        Determine whether the version requirement represented by an edge is
        satisfied by the required node

        Args:
            edge (Requirement): Relationship between two nodes to be tested

        Raises:
            ValueError: Dependency comparator is not valid

        Returns:
            bool: Whether the edge's requirement is satisfied
        """
        try:
            n = self.node_by_name(edge.requires)
        except KeyError:
            return False
        return bool(edge.comparator(n.version, edge.version))

    def resolve(self) -> Tuple[Set[Package], Set[Requirement]]:
        """
        Resolve all dependencies in the graph, iteratively removing nodes with
        unmet dependencies until all remaining requirements are satisfied

        Returns:
            Tuple[Set[Package], Set[Requirement]]: The set of nodes with met
            dependencies and the set of node-edge pairs with unmet dependencies
        """
        failed_deps: Set[Requirement] = set()
        # Test each edge
        failed_edges = {e for e in self.edges if not self.resolve_edge(e)}
        while failed_edges:
            for e in failed_edges:
                # Move requirement to the failed set
                failed_deps.add(e)
                self.edges.remove(e)
                # Try to remove upstream node, but it might already be gone
                try:
                    self.nodes.remove(self.node_by_name(e.required_by))
                except KeyError:
                    pass
            failed_edges = {e for e in self.edges if not self.resolve_edge(e)}
        return self.nodes, failed_deps
