import logging
import time
import uuid
from collections import namedtuple
from typing import Dict, List, Union

log = logging.getLogger("gearbox.log_server")

Log = namedtuple(
    "Log", "context level message timestamp", defaults=("error", "critical", "", "-")
)


class LogBufferServer(logging.StreamHandler):
    """A server to collect logs and buffer them for remote RPC clients"""

    buffers: Dict[str, List[Log]] = {}
    clients = {}

    def __init__(self, max_clients: int = 1):
        """
        Args:
            max_clients (int, optional): Maximum number of clients to support
        """
        self.max_clients = max_clients
        super().__init__()

    def register(self, buffer: list, context) -> Union[str, bool]:
        """
        Register a client's buffer and gRPC request context

        The context can be used to detect whether clients are still connected

        Args:
            buffer (list): A list to which the client still holds a memory reference
            context: The gRPC request context

        Returns:
            Union[str, bool]: A unique registration label on success, else False
        """
        if len(self.clients) >= self.max_clients:
            log.warn("Max log clients reached, rejecting new client")
            return False
        # Register the client and buffer with a unique label
        label = uuid.uuid4()
        self.buffers[label] = buffer
        self.clients[label] = context
        log.debug("Log buffer client attached")
        # Send the client its registration label
        return label

    def unregister(self, label: str):
        """
        Unregister a client's buffer and context

        Args:
            label (str): The label given to the client at registration
        """
        del self.buffers[label]
        del self.clients[label]
        log.debug("Log buffer client detached")

    def emit(self, record):
        """
        Handle log records, buffering them for any clients still connected

        Args:
            record: A Python log record
        """
        # Apply formatters
        message = self.format(record)
        # This is probably bad - the time may have changed already!
        timestamp = time.asctime(time.localtime())
        asctime = f"{timestamp[-4:]} {timestamp[:-5]},{record.msecs}"
        for label, buffer in self.buffers.copy().items():
            buffer.append(Log(record.name, record.levelname, message, asctime))
