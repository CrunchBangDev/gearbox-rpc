import os
from glob import glob
from importlib.util import module_from_spec, spec_from_file_location
from pathlib import Path
from typing import Dict, Generator, List, Sequence

import yaml

from .templates import Cog  # type: ignore


def find_packages(packages_path: str) -> List[str]:
    return glob(str(Path(packages_path).resolve() / "*" / "package.yml"))


def load_cogs_from_package(
    package_path: str, cogs: Sequence[str]
) -> Generator[Cog, None, None]:
    """
    Yield cogs from a given package location

    Args:
        package_path (str): Path from which to attempt loading cogs

    Yields:
        Generator[Cog, None, None]: Cog classes
    """
    package_name = package_path.split(os.sep)[-2]
    spec = spec_from_file_location(package_name, package_path)
    cog_module = module_from_spec(spec)  # type: ignore
    spec.loader.exec_module(cog_module)  # type: ignore
    # Cog modules are required to have an `__all__` property
    errors = []
    for cog_class in cogs:
        try:
            cog = getattr(cog_module, cog_class)
        except AttributeError:
            errors.append(ValueError(f"No cog named {cog_class} in {package_name}"))
            continue
        cog._package = package_name
        yield cog
    if errors:
        raise Exception(errors)  # Raise deferred cog loading errors


def load_cogs(
    packages_path: str, packages: Dict[str, Sequence[str]]
) -> Generator[Cog, None, None]:
    """
    Load packages and their specified cogs

    Args:
        packages_path (str): Path from which to attempt loading packages
        packages (Dict[str, Sequence[str]]): Cog packages and their cog lists

    Returns:
        Generator[Cog, None, None]: Cog classes
    """
    pkg_path = Path(packages_path).resolve()
    for package, exports in packages.items():
        package_path = pkg_path / package / "cogs.py"
        yield from load_cogs_from_package(str(package_path), exports)


def merge_cog_configs(configs) -> dict:
    condensed_config = {}
    for config_file in configs:
        config = (yaml.safe_load(open(config_file)) or {}).get("config") or {}
        package = config_file.split(os.sep)[-2]
        condensed_config.update({f"{package}.{k}": v for k, v in config.items()})
    return condensed_config


def get_main_config() -> dict:
    return yaml.safe_load(open("cog_config.yml")) or {}
