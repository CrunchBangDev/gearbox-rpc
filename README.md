# gearbox

Do all the things!

## Intro

Gearbox is a framework and engine in which to run arbitrary logical widgets called cogs. Data is moved between cogs in a publisher-subscriber model powered by PyPubSub and is structured around data models powered by Django's ORM. A published model is called a topic and cogs can subscribe to it. The cog manager subscribes to all published topics to provide storage for any data that cogs may emit. Subscriber cogs are aware of the data they've processed before and will avoid repeating work, even across application restarts. They'll also try to catch up on work they may have missed while not loaded. Cogs are run concurrently in a daemon process and managed through a web interface. gRPC protobufs are used for communication between the web interface and daemon.

## Setup

```sh
# Download the project
git clone https://gitlab.com/CrunchBangDev/gearbox-rpc
cd gearbox
# Install dependencies
pip install -r requirements.txt
cd src
# Build the gRPC server stub (optional but highly recommended)
python run.py grpc build
# Set up the database
python run.py manage makemigrations
python run.py manage migrate
# Gather cog configurations
python run.py configurecogs
# Start the daemon
python run.py daemon
```

The output should look something like this, with some extra bits:

```
[gearbox:INFO] Started gearbox v0.7.7
```

Congratulations, gearbox is running! Press `Ctrl+C` to exit.

You can also check that the Django web application is working by running:

```sh
python run.py server
```

The output should look something like this:

```
Watching for file changes with StatReloader
[django.utils.autoreload:INFO] Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).
October 28, 2021 - 02:17:53
Django version 3.2.7, using settings 'gearboxweb.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```

If you run both the daemon and server at once, then you should be able to load `http://127.0.0.1:8000/` in a web browser on the same computer.

You will need to create a user account by running

```sh
python run.py manage createsuperuser
```

## Getting Started

TODO: Tell people how to get started

## How Can I Write My Own Cog?

Easy! Create a new folder in the `src/packages` directory and name it something descriptive like `ThingsThatDoImportantStuff`. In that folder, create a file named **`cogs.py`**. In that file, create a class and name it in the same descriptive way, inheriting from the `Cog` template.

```python
from gearbox.templates import Cog


class CogThatDoesImportantStuff(Cog):
    pass
```

Now you have to make some decisions - what will the cog do? If it publishes data for other cogs to consume, we'll need to give it an `emits` property with the models it can emit. Similarly if it subscribes to data from other cogs, we'll need to give it a `collects` property. Let's say our cog collects `Foo`s and emits `Bar`s and `Bat`s - our cog would then look like this:

```python
from gearbox.templates import Cog
from ThingsThatDoImportantStuff.models import Bar, Bat, Foo


class CogThatDoesImportantStuff(Cog):
    collects = Foo
    # We can group multiple models in a tuple
    emits = (Bar, Bat)
```

Now we need to write the logic for how to produce `Bar`s and `Bat`s and what to do when we gets `Foo`s. We can define those as the `main` and `receive` methods, respectively:

```python
# The interrupt allows cogs to shutdown gracefully
# It can also be used to sleep without blocking execution
def main(self, interrupt):
    # Produce some data to publish
    some_data = {"hello": "world"}
    # Package the data using the appropriate model
    payload = Bar(**some_data)
    # Ship it!
    self.emit(payload)
    # You could even do everything in one line
    self.emit(Bat(what_kind_of_bat="the example kind, I guess"))


def receive(self, payload):
    # Payloads are populated Django ORM models
    print(f"Foo received: {payload.something_a_foo_has}")
```

We'll also need to create the models being used here in the same folder in another file named **`models.py`**:

```python
from django.db import models
from gearbox.models import Model


class Foo(Model):
    something_a_foo_has = models.TextField()


class Bar(Model):
    hello = models.TextField()


class Bat(Model):
    what_kind_of_bat = models.TextField()
```

Don't forget to make and run migrations any time you update models, as shown in the Setup section of this document.


The final step to creating your first cog package is to make a **`package.yml`** file to tell GearBox important information about your package:

```yaml
exports:
  - CogThatDoesImportantStuff
```

You will also need to restart the daemon.

Congratulations! You now have a cog! Now try to make another one that emits `Foo`s and/or collects `Bar`s or `Bat`s (be sure to add its name to the `exports` list).

If your cog requires any configuration, you can define keys and default values in a **`config`** section in `package.yml`. After making or changing configs, it's a good idea to run `python run.py configurecogs` and then make sure the values in `src/cog_config.yml` are what you want them to be. The latter file is where settings will be read from when running the cogs.

## History and Goals

There exists no shortage of event-driven Python frameworks, so why did I make this one and why might you want to use it?

I was managing about a dozen cron-driven scripts performing various activities, each of which had similar boilerplate code for talking to a database, reporting errors, and so on. Over time, as the plethora of scripts became less and less manageable, I started to abstract out a lot of the boilerplate into a wrapper for other scripts. Later, as I got tired of managing schedules in crontab, I added scheduling to my system. It was still cron-drive, but it ran every minute and would decide each time which script modules needed to run. As my needs grew, my workflows matured, and I got tired of dealing with various things, I grew this framework to take on all of the things I never want to do manually again.

Most of the existing platforms for doing this kind of thing are large, distributed, and designed to handle massive workloads. The things I do don't need Kafka. They don't even need Rabbitmq. They just need to run periodically, share data seamlessly, and let me know if something goes wrong.

### Simplicity

Design decisions in GearBox optimize for ease of cog development. Making a cog should require very little understanding of the framework, and a cog's functions should be focused entirely on the particular logic it implements and not on satisfying requirements of the framework.

Threading is used instead of async because it provides concurrency for no cost to the developer. If there is an available tradeoff between your time and compute resources, GearBox will try to prefer wasting compute resources. For the same reason, GearBox does not attempt to be a distributed system; shared application state among threads means that you don't need external services to broker worker messaging.

### Portability

One of the primary aims of this project is to help you decouple your domain-specific logic and processes into bite-size modules. These modules should be highly portable, easily migrated to any other automation platform that supports custom Python.

GearBox will happily run on Windows, Linux, or MacOS.

### Maintainability

GearBox tries to make it easy to establish clean division of concerns between code modules, which naturally makes them easier to maintain than the monolithic scripts that tend to exemplify cron-driven workflows.

### Scalability

Although simplicity is prioritized over scalability, the framework supports management of multiple daemon instances through a single web interface. This allows for lateral scaling, though it's important to note that data can't be passed between nodes without being brokered through the database. In practice, it is unlikely that most workloads will require multiple nodes, much less multiple nodes with inter-connected workflows. If your workload requires vertical scalability, this is probably not the framework for you.

### Hackability

Every cog package is registered as a Django app, which makes the web interface highly customizable. For anyone familiar with Django, it should take only minutes to start deploying your own extensions to the existing web interface.

The framework's core code is fairly extensively documented and type-hinted, although there is still work to be done on this front. Efforts have been made to separate the core into multiple components - a scheduler, a state manager, and an API among other things - with the goal of maximizing readability and ease of addition of new functionality.

## References

Documentation for libraries this project makes heavy use of:

 - [PyPubSub](https://pypubsub.readthedocs.io/en/stable/)
 - [Django](https://django.readthedocs.io/en/stable/)
 - [gRPC](https://grpc.io/docs/)

## Disclaimer

This software was developed solely for my personal education and does not have any express purpose. Please do not use this software for any illegal, unethical, immoral, dangerous, reprehensible, uncool, vaguely sketchy, kinda sus, or any other purposes.
